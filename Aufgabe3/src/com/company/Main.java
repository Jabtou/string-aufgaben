package com.company;

public class Main {

    public static void main(String[] args) {
	String student1 = "Hans";
    String student2 = "Paula";
    String student3 = "Patrick";
    String student4 = "Anna";
    String student5 = "Silvan";
    String student6 = "Simon";
    /*Aufgabe3.1*/
        System.out.println(student1.compareTo(student2));
    /*Aufgabe 3.2*/
        System.out.println(student3.compareTo(student4));
    /*Aufgabe 3.3*/
        System.out.println(student5.compareTo(student6));
    /*Aufgabe 3.4*/
        System.out.println(student1.compareTo("Hans"));
    /*Aufgabe 3.5*/
        System.out.println(student1.compareTo("hans"));
    }
}
