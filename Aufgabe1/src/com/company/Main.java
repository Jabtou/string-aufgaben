package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner KeyboardInput = new Scanner(System.in);
        /* Aufgabe 1 a) */
        System.out.print("Bitte etwas eingeben:");
        String KeyboardString = KeyboardInput.nextLine();
        System.out.println(String.format("Länge der Zeichenfolge: %d", KeyboardString.length()));
        /* Aufgabe 1 b) */
        if (KeyboardString.length()%2==0){
            System.out.printf("%s, hat eine Länge von %d Zeichen und ist gerade", KeyboardString, KeyboardString.length());
        }else {
            System.out.println(String.format("%s, hat eine Länge von %d Zeichen und ist ungerade", KeyboardString, KeyboardString.length()));
        }
        /* Aufgabe 1 c) */
        int StringEndBegin = KeyboardString.length() - 1;
        System.out.println("Der Erste Buchstabe in der Zeichenkette ist: " + KeyboardString.substring(0,1));
        System.out.println("Der Letzte Buchstabe in der Zeichenkette ist: " + KeyboardString.substring(StringEndBegin, KeyboardString.length()));

    }
}
